import styled from "styled-components"
import { CSSProperties } from "react"

const Container = styled('div')<{ className?: string }>`
  background-color: white;
  width: 21rem;
  height: 23rem;
  border-radius: 5px;
  padding: 10px;

    .${ props=> props.className + "__nombre"}{
     font-family: sans-serif;
     font-size: 15px;
     color: grey;
  } 

   .${ props=> props.className + "__title"}{
     font-family: sans-serif;
     font-size: 30px;
     color: #ff0068;
  } 

     .${ props=> props.className + "__description"}{
     font-family: sans-serif;
     font-size: 15px;
     color: #3c3a3a;
  } 

    .${ props=> props.className + "__avatar"}{
     width: 3rem;
     height: 3rem;
  } 

    .${ props=> props.className + "__icono"}{
     width: 0.7rem;
     height: 0.7rem;
     margin-left: auto;
  }

  .${ props=> props.className + "__container_header"}{
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 15px;
  }  

  .${ props=> props.className + "__contain"}{
    display: flex;
    justify-content: center;
    align-items: center;
  } 

 .${ props=> props.className + "__label"}{
    background: #ff0068;
    border: none;
    color: white;
    width: 8rem;
    height: 2rem;
    font-family: sans-serif;
    font-size: 12px;
    font-weight: bold;
    border-radius: 5px;
    cursor: pointer;
  }

 .${ props=> props.className + "__container_center"}{
    display: flex;
    justify-content: center;
    margin-bottom: 15px;
  } 

  .${ props=> props.className + "__barra"}{
    width: 15rem;
    height: 2.5rem;
    background: #80808021;
    border-radius: 2px;
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 15px;
  } 

   .${ props=> props.className + "__emojis"}{
    width: 1.5rem;
    height: 1.5rem;
  } 
`

export interface params {
  className?: string
  style?: CSSProperties
  nombre: string
  avatar: string
  icono: string
  title: string
  description: string
  label: string
}

const App = (params: params): JSX.Element => {

  const handleClick = () => { 
    console.log("button") 
  }

  return (
    <Container className={params.className} style={params.style} >

     <div className={params.className + "__contain"}>
      <div className={params.className + "__container_header"}>
        <img src={params.avatar} className={params.className + "__avatar"} />
          <p className={params.className + "__nombre"}>{params.nombre}</p>
     </div>
            <img src={params.icono} className={params.className + "__icono"} />
       </div>

        <p className={params.className + "__title"}>{params.title}</p>
 
        <p className={params.className + "__description"}>{params.description}</p>
      
     <div className={params.className + "__container_center"}>
       <button onClick={handleClick} className={params.className + "__label"}>{params.label}</button>
     </div>
     
     <div className={params.className + "__container_center"}>
        <div className={params.className + "__barra"}>   
          <img src="https://images.emojiterra.com/google/android-10/512px/1f44d.png" className={params.className + "__emojis"} />
          <img src="https://cdn.andro4all.com/andro4all/2020/08/Emoji-1f610-cara-neutral.png" className={params.className + "__emojis"} />
         <img src="https://images.emojiterra.com/google/android-pie/512px/1f44e.png" className={params.className + "__emojis"} />
        </div>
      </div>

    </Container>

  )
}

export default App