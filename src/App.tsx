import styled, { createGlobalStyle } from 'styled-components'
import Card from "./lib"

const Container = styled('div')`

`

export const GlobalStyle = createGlobalStyle`  
  html,
  body {
    margin: 0px;
    background: black;
    display: flex;
    justify-content: center;
    padding: 10px;
  }

  * {
    box-sizing: border-box;
  }
`

const App = () => {

  return (
    <Container>
      <GlobalStyle />
      <Card 
         nombre='Horacio de Wisboo'
         avatar='https://i.pinimg.com/736x/75/e2/08/75e20803480c46fa2c1f89deaa0e8abf.jpg'
         icono='https://img.freepik.com/iconos-gratis/cancelar_318-219174.jpg'
         title='¡Potencia tu curso al máximo!'
         description='Aprende como estructurar el índice y descubre los tipos de contenidos que puedes cargar a tu curso.'      
         label='VER ARTÍCULO'
      />
    </Container>
  )
}

export default App
