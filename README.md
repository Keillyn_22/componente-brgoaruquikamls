# [Template](http://casesandberg.github.io/react-color/)

## Installation & Usage

```sh
npm install @react/brgoaruquikamls --save
```

### Include the Component

```js
import Card from "@react/brgoaruquikamls"

const App = () => {
    return <Card />
}
```  

### Capture
![component](/brgoaruquikamls/capture/componente.png)